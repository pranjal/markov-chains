import pickle
import random

with open("markov_dataset") as f:
    text = f.read().split()

d = {}

for i in range(1, len(text)):
    if text[i-1] not in d.keys():
        d[text[i-1]] = [[], []]

    if text[i] in d[text[i-1]][0]:
        d[text[i-1]][1][d[text[i-1]][0].index(text[i])] += 1
    else:
        d[text[i-1]][0].append(text[i])
        d[text[i-1]][1].append(1)

markov_chain = [random.choice(list(d.keys()))]

for i in range(100):
    if markov_chain[-1] in d.keys() and d[markov_chain[-1]][0]:
        word = random.choices(d[markov_chain[-1]][0], weights=d[markov_chain[-1]][1], k=1)[0]
    else:
        word = random.choice(list(d.keys()))
    markov_chain.append(word)

print(' '.join(markov_chain))
